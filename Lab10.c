#include "msp.h"
#include <stdio.h>
#include "math.h"
#include "LCD.h"

    int count = 0;                  //Globabl variables 
    double V=0;
    double mV=0;
    double temp=0;
    double F = 0;
    char Volt[25] = "Voltage:";     //Chars given names now so there is no need of extra chars
    char T[25] = "Temp:";

/*
*lab10 function 
*Sets up ADC converter
*Uses SysTick interrupt to call rest of lab
*Returns nothing, no need
*/
void lab10()
{

    P8-> SEL0 |= BIT5;
    P8-> SEL1 |= BIT5;
    P8-> DIR &= ~BIT5;

    //Setup for a button that connects to ground
    P5->SEL0 &= ~BIT6; //gpio
    P5->SEL1 &= ~BIT6;
    P5->DIR  &= ~BIT6; //direction is input (0)
    P5->REN  |=  BIT6; //enable resistor
    P5->OUT  |=  BIT6; //set default state to a 1

    // CTL0 Register
    // 31 - 30 00  -> Predivider for 1 divider
    // 29 - 27 000 -> Sample and Hold Source
    // 26      1   -> Automatic (hardware controlled) sampling timer
    // 25      0   -> Don't invert sample and hold signal
    // 24-22   000 -> No clock divider
    // 21-19   100 -> SMCLK (System Clock)
    // 18-17   00  -> Read single channel once
    // 16      0   -> Read only (busy bit)
    // 15-12   0000-> Sample and hold time set to minimum (4 clock cycles) for certain MEM locations
    // 11-8    0000-> Sample and hold time set to maximum (4 clock cycles) for certain MEM locations
    // 7       0   -> Single sample and convert timing
    // 6-5     00  -> Reserved
    // 4       1   -> Turn on ADC
    // 3-2     00  -> Reserved
    // 1       0   -> ADC off until we enable it
    // 0       0   -> ADC conversion off until we start it
    ADC14->CTL0 = 0b00000100001000000000000000010000;

    // CTL1 Register
    // 31-28   0000 -> Reserved
    // 27-24   0000 -> We aren't using internal analog inputs
    // 23      0    -> Internal Temp Sensor is off
    // 22      0    -> Don't divide reference voltage
    // 21      0    -> Reserved
    // 20-16   0000-> Start at MCTL[0] MEM[0]
    // 15-6    00000-> Reserved
    //         00000-> Reserved
    // 5-4     11   -> 14 bit
    // 3       0    -> Unsigned number
    // 2       0    -> Burst mode (not saving power)
    // 1-0     00   -> Regular power mode
    ADC14->CTL1 = 0b110000;

    //MCTL[0]
    // 32-8   All 0 -> Reserved or window comparisons
    // 7      0     -> End of sequence is 0 since we aren't reading a sequence of analog inputs
    // 6-5    00    -> Reserved
    // 4-0    10100 -> Read A20 (decimal 20=0b10100)
    //ADC14->MCTL[0] = 14;
    ADC14->MCTL[0] = 20;

    ADC14->IER0 = 0;
    ADC14->IER1 = 0;

    ADC14->CTL0 |= BIT1;
    ADC14->CTL0 |= BIT0;
//printf("I WORK!!!!!!!!!!!!!!!!!!!!!!!!!");
    commandWrite(0xC0);     //Writes Voltage to LCD
    delay_micro(100);
    writeString(Volt);

    commandWrite(0xD0);     //Writes Temp to LCD
    delay_micro(100);
    writeString(T);

    SysTick->LOAD = 1500000;        //Setting up SysTick to count half second
    SysTick->VAL = 98;
    SysTick->CTRL = (BIT1|BIT0);  //Enable with interrupt

    while(1)            //Holds while for sysTick Interrupt
    {

    }
}

/*
*SysTick Handler
*Runs the states to tell C or F
*Does math and prints to LCD
*Returns nothing because it is an interrupt
*/
void SysTick_Handler()
{
    if(!(ADC14->CTL0&BIT(16)))
           {
               //printf("Counts = %d\n", ADC14->MEM[0]);
               //printf("Counts = %d,    Voltage = %f\n", ADC14->MEM[0], ADC14->MEM[0]/16383.0*3.3);
               V = ADC14->MEM[0]/16383.0*3.3;   //Calcs Voltage
               mV = V*1000;                     //Calcs millivolts
               temp = (mV-500)/10;              //calcs temp in C
               //printf("Temp in C is: %f\n", (ADC14->MEM[0]/16383.0*3.3*1000)-500/10);


               if((P5->IN & BIT6) == 0)            //Checks if buttons is pushed
               {
                   delay_ms(50);                   //debounce
                   while((P5->IN & BIT6) == 0);    //Holds if button is held
                   count++;                        //Incremented count when pushed
               }


               if(count == 0)                           //Starts in C if button is not pressed
               {
                   printf("Voltage is: %0.1f\n", V);    //Printing to Console
                   printf("Temp in C is: %f\n", temp);

                   sprintf(Volt, "%0.1f", V);           //Converting type double to type char
                   commandWrite(0xC9);                  //Writing to correct locations on LCD
                   delay_micro(100);                    //Delay for safety
                   writeString(Volt);                   //Printing value to LCD

                   sprintf(T, "%0.1f", temp);
                   commandWrite(0xD9);
                   delay_micro(100);
                   writeString(T);

                   commandWrite(0xDD);
                   delay_micro(100);
                   commandWrite('°');                   //Finally finding a quick way
                   //dataWrite(0xDF);                     //Another way to print degree symbol

                   commandWrite(0xDF);
                   delay_micro(100);
                   dataWrite(67);

                   ADC14->CTL0 |= BIT0;
                   delay_ms(500);
               }


               if(count == 1)
               {
                   F = temp*9/5+32;                     //Changing C to F

                   printf("Voltage is: %0.1f\n", V);
                   printf("Temp in F is: %f\n", F);

                   sprintf(Volt, "%0.1f", V);           //%.1f to go to one decimal place
                   commandWrite(0xC9);
                   delay_micro(100);
                   writeString(Volt);

                   sprintf(T, "%0.1f", F);
                   commandWrite(0xD9);
                   delay_micro(100);
                   writeString(T);

                   commandWrite(0xDD);
                   delay_micro(100);
                   dataWrite(0xDF);

                   commandWrite(0xDF);
                   delay_micro(100);
                   dataWrite(70);

                   ADC14->CTL0 |= BIT0;
                   delay_ms(500);
               }


               if(count == 2)               //When count is two aka one more than the states wanted reset back to first state
               {
                   count = 0;
               }

           }


}



/*
 * Part 1 function that calls 4 other custom functions
 * returns nothing because it is a void
 */
void Part1()
{
    SysTick_Setup();
    pinSetup();
    delay_ms(60);
    LCD_init();
}


/*
 * Pin set up function
 * Sets up all the pins correctly
 * Returns nothing
 */
void pinSetup()
{
    P4->SEL0 &= ~(0xFC);    //(0b11111100)
    P4->SEL1 &= ~(0xFC);    //GPIO
    P4->DIR |= (0xFC);      //Output
    P4->OUT &= ~(0xFC);     //Initially zero
}

/*
 * SysTick setup function
 * Sets up systick timer correctly
 * reutrns nothing
 */
void SysTick_Setup()
{
    SysTick -> CTRL = 0;            //Disable SysTick During step
    SysTick -> LOAD = 0x00FFFFFF;   //Max reload value
    SysTick -> VAL = 0;             //clears write
    SysTick -> CTRL = 0x00000005;   //Enable systick
}

/*
 * Delay Micro function
 * sets a delay in microseconds
 * Returns nothing
 */
void delay_micro(uint32_t micro)
{
SysTick -> LOAD = (micro*3-1);              //input delay * 3-1
SysTick -> VAL = 0;                         //clears write
while((SysTick -> CTRL & 0x00010000)==0);   //does until value is counted down from
}

/*
 * Delay milli function
 * Sets a delay in milliseconds
 * Returns nothing
 */
void delay_ms(uint32_t milli)
{
    SysTick -> LOAD = (milli*3000-1);           //load a delay of 3000 times sent delay time
    SysTick -> VAL = 0;                         //Clears write
    while((SysTick -> CTRL & 0x00010000)==0);   //Counts down from value to 0
}

/*
 * LCD_init function
 * calls commandwrite and delays
 * returns nothing
 */
void LCD_init(void)         //Initialization for LCD
{
    commandWrite(0x03);     //All values taken from lab instructions
    delay_ms(100);
    commandWrite(0x03);
    delay_micro(200);
    commandWrite(0x03);
    delay_ms(100);
    commandWrite(0x02);

    delay_micro(100);
    commandWrite(0x28);
    delay_micro(100);

    delay_micro(100);
    commandWrite(0x0F);
    delay_micro(100);
    commandWrite(0x01);
    delay_micro(100);
    commandWrite(0x06);
    delay_ms(100);
}

/*
 * Pulse Enable Pin function
 * calls delays
 * Returns nothing
 */
void PulseEnablePin(void)
{
    P4OUT &= ~BIT2;     // sets enable to 0 for output
    delay_micro(20);
    P4OUT |= BIT2;      //Sets enable to 1 for input
    delay_micro(20);
    P4OUT &= ~BIT2;     //enable back to output
    delay_micro(20);
}

/*
 * pushnibble function
 * calls pulseenable
 * returns nothing
 */
void pushNibble(uint8_t nibble)
{
    P4OUT &=~ 0xF0;                 //clears p4.4-p4.7
    P4OUT |= (nibble & 0x0F) << 4;  //pins p4.0-4 wired to D4-D7
    PulseEnablePin();               //calls pulseenable
}

/*
 * Pushbyte function
 * calls nibble twice for 8 bit number
 * reuturns nothing
 */
void pushByte(uint8_t byte)
{
    uint8_t nibble;                 //declares nibble

    nibble = (byte & 0xF0) >> 4;    //saves left 4 numbers of byte and pushes it 4 left
    pushNibble(nibble);             //sends that saved nibble to pushnibble

    nibble = byte & 0x0F;           //saves right 4 numbers of byte to nubble
    pushNibble(nibble);             //sends that new nibble to pushnibble
    delay_micro(100);
}

/*
 * CommandWrite function
 * clears RS and calls pushbyte
 * returns nothing
 */
void commandWrite(uint8_t command)
{
    P4->OUT &= ~BIT3;   //clears RS to 0
    pushByte(command);  //function with command parameter
}

/*
 * dataWrite function
 * sets RS to input and calls pushByte
 * Returns nothing
 */
void dataWrite(uint8_t data)
{
    P4->OUT |= BIT3;    //sets RS to 1
    pushByte(data);     //function with data parameter
}

/*
 * writeString Function
 * prints string that is sent to it
 * returns nothing
 */
void writeString(char *myString)
{
    int i=0;                        //initializing i
    while(myString[i] != '\0')      //loop through until string is done
    {
        dataWrite(myString[i]);     //Writes the character of the string using datawrite
        i++;                        //Gotta increment
    }
}


