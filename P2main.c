#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void scan_value(float *d);          //custom function to scan values
void randNUM(int *f, int *g);       // Custom function that

int main()
{
    int credits;
    int wager;
    int ans;                        // useful variables
    int x;
    int p, c;

    printf("Hello welcome to another gambling game\n\n");
    printf("How many credits do you have?\n\n");
    scan_credits(&credits);
    if(credits <= 0){
        printf("You need a positive amount of credits to play, sorry\n");                   // Small intro credits are first entered here
    exit(0);
    }

    do{

    printf("You have %d credits\n\n", credits);

    if(credits <= 0){                                                                       //checks credits again to make sure they user didn't go under
        exit(0);
    }

    printf("We will both roll a die\nIf your roll is higher than mine you win\n\n");
    printf("How many credits would you like to wager?\n");
    scan_credits(&wager);
        if(wager <= 0){
        printf("You need a positive amount of credits to play, sorry\n");
    exit(0);
    }
        if(wager > credits){
        printf("You can't wager more than you have, sorry\n");                  // exit 0 functions were used to close program if user messed amount up
    exit(0);
    }

    randNUM(&p, &c);                                                    // random number function that will give back two random values


    printf("Your number is %d\n", p);
    printf("My number is %d\n", c);

    if(c > p){
        printf("You lose!\n");
        credits = credits - wager;
        printf("You now have %d credits\n", credits);
    }                                                                           // three if statements for the the three things that could

    if(c < p){
        printf("You win!\n");
        credits = credits + wager;
        printf("You now have %d credits\n", credits);
    }

    if(c == p){
        printf("We tied\n");
    }



    printf("Would you like to play again?\n");
    printf("If yes type 1, if no type any other number\n");                 // 1 must be entered to play again
    scan_credits(&ans);
    }
            while(ans == 1);

    return 0;
}



void scan_credits(int *d)
{

int flag;

do{
    flag = scanf("%d", d);                          // Found this error check from 106 assignment

    if(!flag)
    {
        printf("Please enter a correct value\n");
        fflush(stdin);
    }

}
while(!flag);
}

void randNUM(int *f, int *g)
{

srand(time(0));
*f = rand()%6;

*g = rand()%6;

}
