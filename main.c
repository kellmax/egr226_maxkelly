/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          9/9/2020
*Assignment:    Lab 1: C-Programming Refresher
*File:          main.c
*Description:   Lab objective is to practice and refresh C coding
*/

#include <stdio.h>
#include <stdlib.h>
#include "stats_lib.h";


int main()
{
   FILE *fp;
   int i, k;
   float number[1000];

    if((fp=fopen("data.txt", "r"))== NULL)                         //simple File opener with file check
    {                                                              // Code was obtained from a 106 assignment
        printf("File does not exist");
        exit(-1);
    }

   for (i=0; fscanf (fp, "%f", &number[i]) != EOF; i++);        // EOF means it will go to the end of file

   k = i;

  // printf("%d\n", k); // checks the amount of numbers in file

    printf("Maximum:\t\t%g\n", maximum(number, k));
    printf("Minimum:\t\t%g\n", minimum(number, k));
    printf("Mean:\t\t\t%g\n", mean(number, k));
    printf("Median:\t\t\t%g\n", median(number, k));                         // When each value is needed the function is called in the printf
    printf("Variance:\t\t%g\n", variance(number, k));
    printf("Standard Deviation:\t%g\n", standard_deviation(number, k));



    fclose(fp);

    return 0;
}
