#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

float maximum(float nums[ ], int n);
float minimum (float nums[ ], int n);
float mean(float nums[ ], int n);                           // all the functions are prototyped here
float median(float nums[ ], int n);
float variance(float nums[ ], int n);
float standard_deviation(float nums[ ], int n);


#endif // TEST_H_INCLUDED
