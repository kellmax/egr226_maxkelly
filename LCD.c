#include "LCD.h"
#include "msp.h"

/*
 * Part 1 function that calls 4 other custom functions
 * returns nothing because it is a void
 */
void Part1()
{
    SysTick_Setup();
    pinSetup();
    delay_ms(60);
    LCD_init();
}

/*
 * Part 2 function that declares the strings and calls three other functions
 * Calls commandWrite, delay_micro, and writeString
 * Returns nothing
 */
void Part2()
{
char FirstName[50] = "Max";     //4 strings for part two
char LastName[50] = "Kelly";
char ClassType[50] = "EGR";
char ClassNum[50] = "226";


commandWrite(0x86);         //Writes to position on LCD
delay_micro(100);           //Delays
writeString(FirstName);     //calls write string function to print

commandWrite(0xC6);
delay_micro(100);
writeString(LastName);

commandWrite(0x96);
delay_micro(100);
writeString(ClassType);

commandWrite(0xD6);
delay_micro(100);
writeString(ClassNum);
}

/*
 * Part 3 function that scrolls through string
 * calls commandWrite, delay_ms, and datawrite
 * Returns nothing
 */
void Part3()
{
int i=0;
int j=0;                                //ints and chars initiallized
char word[50] = "LABORATORY OVER ";

while(1)                                //while 1 to repeat forever
{
    commandWrite(0x80);                 //Writes to position on LCD (top left)
    delay_ms(500);                      //Delayed half a second

        for(i=j; i<strlen(word); i++)   //For loop that it will enter per letter in the string
            {                           //strlen used instead of hard coded 16 in so ideally string could be changed
                dataWrite(word[i]);     //the string prints here
            }
        j++;      //j++ so that the for loop repeats starting the string in the top left corner of the next letter each increase

    if(j==16)                           //this 16 hardcoded so it completely runs through an entire line
    {
        commandWrite(0x8F);             //Postions to top right of LCD
        delay_ms(500);                  //gotta have that delay
        for(i=0; i <strlen(word)-1; i++)// strlen -1 because I noticed it was going one to far while scrolling in
        {                               //this seemed to fix it
            dataWrite(word[i]);         //write the string starting at letter [i] in top left
            commandWrite(0x18);         //Command to shift entire display left
            delay_ms(500);              //Delayed half a second
        }

        commandWrite(0x01);             //Clears display
        commandWrite(0x80);             //Writes back to top rights
        j=0;                            //j must be reset to 0 in order to repeat scrolling

    }
}
}

/*
 * Pin set up function
 * Sets up all the pins correctly
 * Returns nothing
 */
void pinSetup()
{
    P4->SEL0 &= ~(0xFC);    //(0b11111100)
    P4->SEL1 &= ~(0xFC);    //GPIO
    P4->DIR |= (0xFC);      //Output
    P4->OUT &= ~(0xFC);     //Initially zero
}

/*
 * SysTick setup function
 * Sets up systick timer correctly
 * reutrns nothing
 */
void SysTick_Setup()
{
    SysTick -> CTRL = 0;            //Disable SysTick During step
    SysTick -> LOAD = 0x00FFFFFF;   //Max reload value
    SysTick -> VAL = 0;             //clears write
    SysTick -> CTRL = 0x00000005;   //Enable systick
}

/*
 * Delay Micro function
 * sets a delay in microseconds
 * Returns nothing
 */
void delay_micro(uint32_t micro)
{
SysTick -> LOAD = (micro*3-1);              //input delay * 3-1
SysTick -> VAL = 0;                         //clears write
while((SysTick -> CTRL & 0x00010000)==0);   //does until value is counted down from
}

/*
 * Delay milli function
 * Sets a delay in milliseconds
 * Returns nothing
 */
void delay_ms(uint32_t milli)
{
    SysTick -> LOAD = (milli*3000-1);           //load a delay of 3000 times sent delay time
    SysTick -> VAL = 0;                         //Clears write
    while((SysTick -> CTRL & 0x00010000)==0);   //Counts down from value to 0
}

/*
 * LCD_init function
 * calls commandwrite and delays
 * returns nothing
 */
void LCD_init(void)         //Initialization for LCD
{
    commandWrite(0x03);     //All values taken from lab instructions
    delay_ms(100);
    commandWrite(0x03);
    delay_micro(200);
    commandWrite(0x03);
    delay_ms(100);
    commandWrite(0x02);

    delay_micro(100);
    commandWrite(0x28);
    delay_micro(100);

    delay_micro(100);
    commandWrite(0x0F);
    delay_micro(100);
    commandWrite(0x01);
    delay_micro(100);
    commandWrite(0x06);
    delay_ms(100);
}

/*
 * Pulse Enable Pin function
 * calls delays
 * Returns nothing
 */
void PulseEnablePin(void)
{
    P4OUT &= ~BIT2;     // sets enable to 0 for output
    delay_micro(20);
    P4OUT |= BIT2;      //Sets enable to 1 for input
    delay_micro(20);
    P4OUT &= ~BIT2;     //enable back to output
    delay_micro(20);
}

/*
 * pushnibble function
 * calls pulseenable
 * returns nothing
 */
void pushNibble(uint8_t nibble)
{
    P4OUT &=~ 0xF0;                 //clears p4.4-p4.7
    P4OUT |= (nibble & 0x0F) << 4;  //pins p4.0-4 wired to D4-D7
    PulseEnablePin();               //calls pulseenable
}

/*
 * Pushbyte function
 * calls nibble twice for 8 bit number
 * reuturns nothing
 */
void pushByte(uint8_t byte)
{
    uint8_t nibble;                 //declares nibble

    nibble = (byte & 0xF0) >> 4;    //saves left 4 numbers of byte and pushes it 4 left
    pushNibble(nibble);             //sends that saved nibble to pushnibble

    nibble = byte & 0x0F;           //saves right 4 numbers of byte to nubble
    pushNibble(nibble);             //sends that new nibble to pushnibble
    delay_micro(100);
}

/*
 * CommandWrite function
 * clears RS and calls pushbyte
 * returns nothing
 */
void commandWrite(uint8_t command)
{
    P4->OUT &= ~BIT3;   //clears RS to 0
    pushByte(command);  //function with command parameter
}

/*
 * dataWrite function
 * sets RS to input and calls pushByte
 * Returns nothing
 */
void dataWrite(uint8_t data)
{
    P4->OUT |= BIT3;    //sets RS to 1
    pushByte(data);     //function with data parameter
}

/*
 * writeString Function
 * prints string that is sent to it
 * returns nothing
 */
void writeString(char *myString)
{
    int i=0;                        //initializing i
    while(myString[i] != '\0')      //loop through until string is done
    {
        dataWrite(myString[i]);     //Writes the character of the string using datawrite
        i++;                        //Gotta increment
    }
}


