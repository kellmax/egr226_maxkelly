/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          10/28/2020
*Assignment:    Lab 8: PWM with keypad and DC motor
*File:          main.c
*Description:   Lab objective is to use PWM with a DC motor and connect the keypad to it
*/


#include "msp.h"
#include "math.h"
#include "pwm.h"


/**
 * main.c
 */


void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    Part1
    //Part1();

    Part2
    //Part2();

    //Part3
    Part3();
}
