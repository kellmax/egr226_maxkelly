/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          9/15/2020
*Assignment:    Lab 2: C-Programming Refresher Part - Structured Programming
*File:          main.c
*Description:   Lab objective is to practice and refresh C coding and develop searchable database using structs
*/

#include <stdio.h>
#include <stdlib.h>             //Libraries including the string libs for my strings
#include <string.h>


   typedef struct {             //Creating my structure
   char title[225];
   char author_name[50];
   char ISBN[10];
   int pages[10];
   int year_published[5];
   }book_struct;


   int parse_file(char filename[], book_struct book_array[]);           //All of my custom functions prototyped
   int print_book(book_struct the_array[], int k);
   int search_title(book_struct array[], int n, char input[]);
   int search_author(book_struct array[], int n, char input[]);
   int search_ISBN(book_struct array[], int n, char input[]);
   void scan_value(int *d);





int main()
{
int search, i, j;                           //Some integers and chars and arrays, just the basics
char input_from_user[50];
char Filename[20];
book_struct my_book[360];

parse_file(Filename, my_book);                                      //Calling my parse_file to read in file
j = parse_file(Filename, my_book);                                  //Used this more for a check to get a value of how many books there are
//printf("%d\n", j); This will print how many books are in the file

do{

printf("What would you like to search by?\n[0] Title\n[1] Author\n[2] ISBN\n"); //prompting user
scan_value(&search);                                                            //scan function used to see if its a number

    if(search == 0)                                                             //On all ifs I check 012 because they could enter any # and get past first error check
    {
        printf("Please enter the book title, we are case sensative\n");         //Had to make sure user knows its case sensative
        fgets(input_from_user, 50, stdin);
        scanf("%[^\n]s",input_from_user);               //Zayn from Quora helped with scanning in value
                for(i=0;i<j;i++)                        //Found a use for j. This way it runs through each book and info
                {
                        if(search_title(my_book, i, input_from_user))           //Sends to the search function
                        {
                        print_book(my_book, i);                                 //Sends to the print function
                        }
                }
    }
    if(search == 1)
    {
          printf("Please enter the author, we are case sensative\n");
        fgets(input_from_user, 50, stdin);
        scanf("%[^\n]s",input_from_user);               //Zayn from Quora helped with scanning in value
                for(i=0;i<j;i++)
                {
                        if(search_author(my_book, i, input_from_user))
                        {
                        print_book(my_book, i);
                        }
                }
    }
    if(search == 2)
    {
        printf("Please enter the ISBN\n");
        fgets(input_from_user, 50, stdin);
        scanf("%[^\n]s",input_from_user);               //Zayn from Quora helped with scanning in value
                for(i=0;i<j;i++)
                {
                        if(search_ISBN(my_book, i, input_from_user))
                        {
                        print_book(my_book, i);
                        }
                }
    }

    printf("Would you like to search again?\n");
    printf("If yes type 1, if no type any other number\n");                 // 1 must be entered to search again
    scan_value(&search);

        }
            while(search == 1);

    return 0;
}


int parse_file(char filename[],book_struct book_array[]){
 FILE *fp;

    if((fp=fopen("BookList.csv", "r"))== NULL)                      //simple File opener with file check
    {                                                              // Code was obtained from a 106 assignment
        printf("File does not exist");
        exit(-1);
    }

    char buffer[512];                                              //This part of the code was taken from the demo on bb
    int i = 0;

    while (fgets(buffer, 512, fp)){
        char * ptr = strtok(buffer,",");
        if(strcmp(ptr,"N/A"))
            strcpy(book_array[i].title,ptr);

        ptr = strtok(NULL,",\n");
        if(strcmp(ptr, "N/A"))
            strcpy(book_array[i].author_name,ptr);

        ptr = strtok(NULL,",\n");                               //At this point the demo cuts but I found it best to just continue to read in as strings
        //if(strcmp(ptr, "N/A"))
            strcpy(book_array[i].ISBN,ptr);

        ptr = strtok(NULL,",\n");                               //I read in as strings because even if its suppose to be all numbers letters were thrown in
        //if(strcmp(ptr, "N/A"))
            strcpy(book_array[i].pages,ptr);

        ptr = strtok(NULL,",\n");
        //if(strcmp(ptr, "N/A"))
            strcpy(book_array[i].year_published,ptr);

        i++;
    }
       fclose(fp);                                              //Can't forget to close file
       return i;
}

 int print_book(book_struct the_array[], int k)                                 //Simple print function that did it in the style I wanted
    {
    printf("\n|Title:\t\t\t%s\n", the_array[k].title);
    printf("|Author:\t\t%s\n", the_array[k].author_name);
    printf("|ISBN:\t\t\t%s\n", the_array[k].ISBN);
    printf("|Pages:\t\t\t%s\n", the_array[k].pages);
    printf("|Year Published:\t%s\n\n\n\n\n\n", the_array[k].year_published);
   }

      int search_title(book_struct the_array[], int n, char input[])           //All search files are alike
      {
            if(strstr(the_array[n].title,input));                              //We learned this strstr from a cs kid in lecture
      }

      int search_author(book_struct the_array[], int n, char input[])
      {
            if(strstr(the_array[n].author_name,input));                        //It worked very well
      }

      int search_ISBN(book_struct the_array[], int n, char input[])
      {
            if(strstr(the_array[n].ISBN,input));
      }

      void scan_value(int *d)                       // Simple scan value and error check
{
int flag;

do{
    flag = scanf("%d", d);                          // Found this error check from 106 assignment

    if(!flag)
    {
        printf("Please enter a correct value\n");   // The only thing is this error check only checks to see if its a digit
        fflush(stdin);
    }
}
while(!flag);
}
