/*
 * LCD.h
 *
 *  Created on: Oct 20, 2020
 *      Author: Max Kelly
 */

#ifndef LCD_H_
#define LCD_H_

#include "msp.h"


void Part1(void);
void Part2(void);
void Part3(void);
void pinSetup(void);
void SysTick_Setup();
void delay_micro(uint32_t micro);
void delay_ms(uint32_t milli);
void LCD_init(void);
void PulseEnablePin (void);
void pushNibble (uint8_t nibble);
void pushByte (uint8_t byte);
void commandWrite(uint8_t command);
void dataWrite(uint8_t data);
void writeString(char *myString);


#endif /* LCD_H_ */
