#include <stdio.h>
#include <math.h>
#include "stats_lib.h"

                                                            // Where all the math is and solving is

float maximum(float nums[ ], int n){

int max = -INFINITY;
int j;

for(j=1; j<n; j++){
    if(nums[j] > max)
        max = nums[j];
}
    return max;
}

                                                        // max and min both start high or low and then go through points comparing
float minimum (float nums[ ], int n){

int min = INFINITY;
int j;

for(j=1; j<n; j++){
    if(nums[j] < min)
        min = nums[j];
}
    return min;

}


float mean(float nums[ ], int n){

float last = 0;
int i;
float avg = 0;

for(i=0; i<n; i++)
{
    last = nums[i]+last;                        // mean is obviously adding up and divide by n
}

avg = last/n;

return avg;
}


float median(float nums[ ], int n){

float med, myvar;
int m, i;

for(m=0; m <=n; m++)
{
    for(i=m+1; i<n; i++)
    {
        if(nums[i] < nums[m])
        {
            myvar = nums[m];                      // sorts array and then to get to the middle divide by two
            nums[m] = nums[i];
            nums[i] = myvar;
        }
    }
}

med = nums[n/2];

return med;
}


float variance(float nums[ ], int n){

float var=0;


float Sum=0.0, Mean, myvar;
int i;                                          // quick calc to get mean again, probably should have called it

for (i=0; i<n; i++){
    Sum += nums[i];
}
Mean = Sum/n;
for (i=0; i<n; i++){
myvar = myvar + pow(nums[i]-Mean,2);
}                                                  // finding the summation and dividing by n-1

var = myvar/(n-1);

return var;
}


float standard_deviation(float nums[ ], int n){

float SD;
float vari;
                                                    // call the variance function and square root it
vari = variance(nums, n);

SD = sqrt(vari);

return SD;
}



