#include "keypad.h"
#include "msp.h"
/*
 * keypad.c
 *
 *  Created on: Oct 22, 2020
 *      Author: Max Kelly
 */


void setupKeypad(void)
{
        P9->SEL0 &= ~(0x7F);    //(0b01111111)
        P9->SEL1 &= ~(0x7F);    //GPIO
        P9->DIR &= ~(0x7F);      //Output
        P9->REN |= (0x7F);      //Pull up resistor enabled
        P9->OUT |= (0x7F);     //Initially zero
}


int getLastKeyPress()
{
    int val = -1;

                P9->OUT &= ~BIT4;  //Set default state to a 0
                P9->DIR |=  BIT4;  //Change to an output (that is 0 due to previous line)
                                                                                            //Each button acts the same so I will comment one
                if((P9->IN & BIT0) == 0)                //If the button is pressed
                {
                    delay_ms(100);                      //SysTick delay
                    val = 1;
                    while((P5->IN & BIT2) == 0);        //Lock as long as button is pressed so there is no reprints
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 4;
                    while((P5->IN & BIT0) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 7;
                    while((P1->IN & BIT7) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 20;
                    while((P1->IN & BIT6) == 0);
                }

                P9->OUT |=  BIT4;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT4;  //Change to an input

                P9->OUT &= ~BIT5;  //Set default state to a 0
                P9->DIR |=  BIT5;  //Change to an output (that is 0 due to previous line)

                if((P9->IN & BIT0) == 0)
                {
                    delay_ms(100);
                    val = 2;
                    while((P5->IN & BIT2) == 0);
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 5;
                    while((P5->IN & BIT0) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 8;
                    while((P1->IN & BIT7) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 0;
                    while((P1->IN & BIT6) == 0);
                }

                P9->OUT |=  BIT5;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT5;  //Change to an input

                P9->OUT &= ~BIT6;  //Set default state to a 0
                P9->DIR |=  BIT6;  //Change to an output (that is 0 due to previous line)

                if((P9->IN & BIT0) == 0)
                {
                    delay_ms(100);
                    val = 3;
                    while((P5->IN & BIT2) == 0);
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 6;
                    while((P5->IN & BIT0) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 9;
                    while((P1->IN & BIT7) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 30;
                    while((P1->IN & BIT6) == 0);
                }

                P9->OUT |=  BIT6;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT6;  //Change to an input

    return val;
}

void delay_ms(float ms)        //delays in milliseconds
{
    SysTick->LOAD = 3000*ms-1;          //Load is value we count to
                                        // -1 because we start at 0
    SysTick->VAL  = 0;                  //Starting value
    SysTick->CTRL |= BIT2|BIT0;                 //Turns the timer on
    while(!(SysTick->CTRL & 0x00010000));       //while its on and not equal to load value
    SysTick->CTRL = 0;                          //Turn off SysTick
}
