/*
 * pwm.h
 *
 *  Created on: Oct 25, 2020
 *      Author: Max Kelly
 */

#ifndef PWM_H_
#define PWM_H_

#include "msp.h"


void Part1();
void Part2();
void Part3();
void SysTick_Setup();
void delay_Ms(uint32_t milli);

void setupKeypad(void);
int getLastKeyPress();
void delay_ms(float ms);


#endif /* PWM_H_ */
