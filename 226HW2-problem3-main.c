#include "msp.h"
#include <stdio.h>
#include <stdlib.h>

#include "keypad.h"

    void setupKeypad();
    int getLastKeyPress();



void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer
    setupKeypad();
    int key;
    int time_count = 0;

    // Using TimerA0 as a 1 second timer
    TIMER_A0->CTL =  0b1011010110;  //Up Mode, Divide by 8, SMCLK, interrupt enabled
    TIMER_A0->EX0 = 0b111; //Divide by 8... with the above line, clock is now 3000000/8/8 = 46875 Hz
    TIMER_A0->CCR[0] = 46875; //Will count through once a second

//    Removed SysTick initialization to allow you to use it in your keypad.
//    Using TimerA as a counter in main() as an example for PWM and Interrupts
//    SysTick->LOAD = 15000000;  //5 sec at 3MHz  //Reload register STRVR
//    SysTick->VAL = 226; //any value clears count
//    SysTick->CTRL = BIT0;  //Enable   //Status and Control Register STCSR

    while(1)
    {
        key = getLastKeyPress();  //get keypress
        if(key != -1)
        {
            printf("Key = %d\n",key);
        }

        if(TIMER_A0->CTL&BIT0)  // if interrupt flag is set, counter has finished
        {
            TIMER_A0->CTL &= ~BIT0; // Reset interrupt flag for next counter finish
            time_count++; // Count one more second.
            if(time_count > 5) // 5 seconds have passed
            {
                time_count = 0;  // Reset time count back to 0 to start counting again
                printf("5 seconds have passed\n\n");
            }
        }
    }
}
