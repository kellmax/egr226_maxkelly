/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          10/21/2020
*Assignment:    Lab 9: Using GPIO interrupts to control PWM and 7_Segment LED
*File:          main.c
*Description:   To familiarize with interrupts and 7_Segment LED
*/

#include "msp.h"
#include "math.h"
#include "7SegLED.h"


/**
 * main.c
 */
//void delay_ms(float ms);

void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer


	//Part1();

	//Part2();

	Part3();

}


