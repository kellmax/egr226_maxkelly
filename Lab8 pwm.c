#include "pwm.h"
#include "msp.h"
#include "math.h"

/*
 * pwm.c
 *
 *  Created on: Oct 25, 2020
 *      Author: Max Kelly
 */

/*
 * Part 1 function Sets up pins and calls systick
 * returns nothing because it is a void
 */
void Part1()
{
    P6 -> SEL0 &= ~BIT7;    //GPIO
    P6 -> SEL1 &= ~BIT7;
    P6 -> DIR |= BIT7;
    SysTick_Setup();        //Sets up systick

    float T;                //Variables declared to solve for Ton and Toff
    float DC;
    float Ton;
    float Toff;

    T = 25;
    DC = 35;    //Percent you want duty cycles

    while(1)
    {
        Ton = (DC*T)/100;
        Toff = T - Ton;
        P6 -> OUT |= BIT7;  //Turn on
        delay_Ms(Ton);      //Delay on (Ton)
        P6 -> OUT &= ~BIT7; //Turn off
        delay_Ms(Toff);     //Delay off (Toff)
    }
}

/*
 * Part 2 function Runs PWM with TIMER_A
 * returns nothing because it is a void
 */
void Part2()
{
    float DC=0;
    P6 -> SEL0 |=  BIT6;
    P6 -> SEL1 &= ~BIT6;
    P6 -> DIR  |=  BIT6;

    TIMER_A2 -> CTL = 0b1000010100;
    TIMER_A2 -> CCR[0] = 37500-1;        //period
    TIMER_A2 -> CCTL[3] = 0b11100000;    //Reset/Set
    TIMER_A2 -> EX0 = 0b001;

    while(1){

    TIMER_A2 -> CCR[3] = 37500*DC;       //50% duty cycle
    }
}

/*
 * Part 3 function just like part 2 but runs keypad in while
 * returns nothing because it is a void
 */
void Part3()
{

    setupKeypad();

    P6 -> SEL0 |=  BIT6;
    P6 -> SEL1 &= ~BIT6;
    P6 -> DIR  |=  BIT6;

    TIMER_A2 -> EX0 = 0b001;
    TIMER_A2 -> CTL = 0b1000010100;
    TIMER_A2 -> CCR[0] = 37500-1;        //period
    TIMER_A2 -> CCTL[3] = 0b11100000;   //Reset/Set

    while (1)
    {
        getLastKeyPress();

    }

}

/*
 * SysTick setup function
 * Sets up systick timer correctly
 * reutrns nothing
 */
void SysTick_Setup()
{
    SysTick -> CTRL = 0;            //Disable SysTick During step
    SysTick -> LOAD = 0x00FFFFFF;   //Max reload value
    SysTick -> VAL = 0;             //clears write
    SysTick -> CTRL = 0x00000005;   //Enable systick
}

/*
 * Delay milli function
 * Sets a delay in milliseconds
 * Returns nothing
 */
void delay_Ms(uint32_t milli)
{
    if(milli != 0)
    {
    SysTick -> LOAD = (milli*3000-1);           //load a delay of 3000 times sent delay time
    SysTick -> VAL = 0;                         //Clears write
    while((SysTick -> CTRL & 0x00010000)==0);   //Counts down from value to 0
    }

    if(milli == 0);
}

/*
*Pin setup initializes all pins correctly
*No parameters, not needed
*No return has nothing to give back
*/
void setupKeypad(void)
{
        P9->SEL0 &= ~(0x7F);    //(0b01111111)
        P9->SEL1 &= ~(0x7F);    //GPIO
        P9->DIR &= ~(0x7F);      //Output
        P9->REN |= (0x7F);      //Pull up resistor enabled
        P9->OUT |= (0x7F);     //Initially zero
}

/*
*getLastKeyPress function reads through a button being pressed, once function gets here it stays here
*no parameters, nothing is sent here
*no return, nothing to get from it, it all stays here
*/
int getLastKeyPress()
{
    int val;

                P9->OUT &= ~BIT4;  //Set default state to a 0
                P9->DIR |=  BIT4;  //Change to an output (that is 0 due to previous line)
                                                                                            //Each button acts the same so I will comment one
                if((P9->IN & BIT0) == 0)                //If the button is pressed
                {
                    delay_ms(100);                      //SysTick delay
                    val = 1;
                    while((P9->IN & BIT0) == 0);        //Lock as long as button is pressed so there is no reprints
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 4;
                    while((P9->IN & BIT1) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 7;
                    while((P9->IN & BIT2) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 0;
                    while((P9->IN & BIT3) == 0);
                }

                P9->OUT |=  BIT4;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT4;  //Change to an input

                P9->OUT &= ~BIT5;  //Set default state to a 0
                P9->DIR |=  BIT5;  //Change to an output (that is 0 due to previous line)

                if((P9->IN & BIT0) == 0)
                {
                    delay_ms(100);
                    val = 2;
                    while((P9->IN & BIT0) == 0);
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 5;
                    while((P9->IN & BIT1) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 8;
                    while((P9->IN & BIT2) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 0;
                    while((P9->IN & BIT3) == 0);
                }

                P9->OUT |=  BIT5;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT5;  //Change to an input

                P9->OUT &= ~BIT6;  //Set default state to a 0
                P9->DIR |=  BIT6;  //Change to an output (that is 0 due to previous line)

                if((P9->IN & BIT0) == 0)
                {
                    delay_ms(100);
                    val = 3;
                    while((P9->IN & BIT0) == 0);
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 6;
                    while((P9->IN & BIT1) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 9;
                    while((P9->IN & BIT2) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 0;
                    while((P9->IN & BIT3) == 0);
                }

                P9->OUT |=  BIT6;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT6;  //Change to an input

                printf("%d\n", val);

                TIMER_A2 -> CCR[3] = 3750*val;    // duty cycle
    return val;
}

/*
 * Delay milli function
 * Sets a delay in milliseconds
 * Returns nothing
 */
void delay_ms(float ms)        //delays in milliseconds
{
    SysTick->LOAD = 3000*ms-1;          //Load is value we count to
                                        // -1 because we start at 0
    SysTick->VAL  = 0;                  //Starting value
    SysTick->CTRL |= BIT2|BIT0;                 //Turns the timer on
    while(!(SysTick->CTRL & 0x00010000));       //while its on and not equal to load value
    SysTick->CTRL = 0;                          //Turn off SysTick
}
