/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          12/13/2020
*Assignment:    Final Project
*File:          main.c
*Description:   A culmination of what was learned in 226 put into a project
*/

#include "msp.h"
#include <stdio.h>
#include <stdlib.h>

#include "project.h"

/**
 * main.c
 */


void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer


    //Setup for a button that connects to ground
    P5->SEL0 &= ~BIT4; //gpio to P5.6
    P5->SEL1 &= ~BIT4;
    P5->DIR  &= ~BIT4; //direction is input (0)
    P5->REN  |=  BIT4; //enable resistor
    P5->OUT  |=  BIT4; //set default state to a 1

    //Setup functions for LCD
    SysTick_Setup();
    pinSetup();
    delay_ms(60);
    LCD_init();

    //TA2.1 PWM pin setup
    P5 -> SEL0 |=  BIT6;
    P5 -> SEL1 &= ~BIT6;
    P5 -> DIR  |=  BIT6;

    //PWM controls
    TIMER_A2 -> CTL = 0b1000010100;
    TIMER_A2 -> CCR[0] = 60000;        //period
    TIMER_A2 -> CCTL[1] = 0b11100000;   //Reset/Set
    TIMER_A2 -> CCR[1] = 60000;    // duty cycle

    //ADCd pin setup
    P8-> SEL0 |= BIT5;
    P8-> SEL1 |= BIT5;
    P8-> DIR &= ~BIT5;

    //ADC registers
    ADC14->CTL0 = 0;
    ADC14->CTL0 |= 0b00000100001000000000000000010000;
    ADC14->CTL1 |= 0b110000;
    ADC14->MCTL[0] = 20;
    ADC14->CTL0 |= BIT1;
    ADC14->CTL0 |= BIT0;



    //Chars created and changed to put on LCD (can only be 16 characters long)
    char mainM[50] = "Main Menu ";     //4 strings for part two
    char door[50] = "[1] Door";
    char motor[50] = "[2] Motor";
    char lights[50] = "[3] Lights";

    char A[50] = "   Authorized   ";
    char O[50] = "     Only";





    //Function to set up keypad
    setupKeypad();

        //Variables used to move through while loops and getting key press
       int key;
       int time_count = 0;
       //char k[100];
       int ENTER = 0;

       // Using TimerA0 as a 1 second timer
       TIMER_A0->CTL =  0b1011010110;  //Up Mode, Divide by 8, SMCLK, interrupt enabled
       TIMER_A0->EX0 = 0b111; //Divide by 8... with the above line, clock is now 3000000/8/8 = 46875 Hz
       TIMER_A0->CCR[0] = 46875; //Will count through once a second

   //    Removed SysTick initialization to allow you to use it in your keypad.
   //    Using TimerA as a counter in main() as an example for PWM and Interrupts
   //    SysTick->LOAD = 15000000;  //5 sec at 3MHz  //Reload register STRVR
   //    SysTick->VAL = 226; //any value clears count
   //    SysTick->CTRL = BIT0;  //Enable   //Status and Control Register STCSR


       //Very important if interrupts are to work
       NVIC_EnableIRQ(PORT3_IRQn);
       __enable_interrupt();


       //Overarching while 1 loop to forever lock the code in
       while(1)
       {


           //Lock screen saver
           commandWrite(0x80);         //Writes to position on LCD
           delay_micro(100);           //Delays
           writeString(A);

           commandWrite(0xC0);         //Writes to position on LCD
           delay_micro(100);           //Delays
           writeString(O);

           if(!(P5->IN & BIT4) == 0)                    //If touch is sensed
              {
                  delay_ms(50);                         //debounce

                  printf("The touch sensor works\n");   //Check to make sure touch is pressed

                  ENTER = 1;                            //Change Enter to 1 to go into main menu while loop
                  CLEAR();                              //Clears the lcd
              }

           while(ENTER == 1)            //goes in while loop once sensor is sensed
           {


               //Main menu wrote to the LCD
           commandWrite(0x80);         //Writes to position on LCD
           delay_micro(100);           //Delays
           writeString(mainM);         //calls write string function to print

           commandWrite(0xC0);
           delay_micro(100);
           writeString(door);

           commandWrite(0x90);
           delay_micro(100);
           writeString(motor);


           commandWrite(0xD0);
           delay_micro(100);
           writeString(lights);

           key = getLastKeyPress();  //get keypress

           //getkeypress always returns -1 unless something is pressed
           if(key != -1)
           {
               CLEAR();                     //clear lcd

               printf("Key = %d\n",key);    //self check


               if(key == 1)
               {
                   Door();          //Door function
               }

               if(key == 2)
               {
                   Motor();         //Motor Function
               }

               if(key == 3)
               {
                   Lights();        //Lights function
               }

               if(key == 20)
               {
                   ENTER = 0;       // * is equal to 20 so when key is 20 that means * is pressed
               }

           }



           if(TIMER_A0->CTL&BIT0)  // if interrupt flag is set, counter has finished
           {
               TIMER_A0->CTL &= ~BIT0; // Reset interrupt flag for next counter finish
               time_count++; // Count one more second.
               if(time_count > 5) // 5 seconds have passed
               {
                   time_count = 0;  // Reset time count back to 0 to start counting again
                   printf("5 seconds have passed\n\n");
               }
           }


       }
       }



}

int L = 0;              //initializes L once in the code to start on for RGB

void PORT3_IRQHandler()
{

    if((P3->IN & BIT5) == 0)            //if button is pressed
    {
        delay_ms(50);                   //debounce
        //while((P3->IN & BIT5) == 0);
        TIMER_A2 -> CCR[3] = 0;         //set speed of motor to 0

    }


    if((P3->IN & BIT7) == 0)            //if button is pressed
    {
        delay_ms(50);                   //deboucne
        //while((P3->IN & BIT7) == 0);


        if(L==0)                        //changes through L bing 0 and 1
        {
            P5 -> DIR  &=  ~BIT7;
            P6 -> DIR  &=  ~BIT7;       //Easy way to turn all LEDs off
            P10 -> DIR &=  ~BIT5;
        }

        if(L==1)
        {
            P5 -> DIR  |=  BIT7;
            P6 -> DIR  |=  BIT7;        //Turns all LEDs back on without changing PWM value for brihtness
            P10 -> DIR |=  BIT5;

            L = -1;                     //resets L to -1 so when increased it is 0 and will enter first if statement again
        }

        L++;                            //once pressed RGB will turn on or off then increase by one

    }


    P3 -> IFG = 0;                      //very important to clear flag for port 3
}

