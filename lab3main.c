/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          9/23/2020
*Assignment:    Lab 3: Getting Familiar with CCS and MSP
*File:          lab3main.c
*Description:   To learn basic skills of CCS and the MSP (blinking a LED)
*/

#include "msp.h"


/**
 * main.c
 */
void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	// Configure GPIO for output on P1.0 LED1 on MSP432 Launchpad
	P1->DIR = BIT0;

	//Temporary variable for loop-maintenance
	int i;
	int j;
	while(1){
	    P1->OUT ^= BIT0;        // Toggle LED status
	    for(i=j; i>0; i--); // C
	}
}
