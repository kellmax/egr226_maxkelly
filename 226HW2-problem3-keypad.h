/*
 * keypad.h
 *
 *  Created on: Oct 22, 2020
 *      Author: Max Kelly
 */

#ifndef KEYPAD_H_
#define KEYPAD_H_

void setupKeypad(void);
int getLastKeyPress();
void delay_ms(float ms);

#endif /* KEYPAD_H_ */
