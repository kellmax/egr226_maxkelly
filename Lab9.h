/*
 * 7SegLED.h
 *
 *  Created on: Nov 2, 2020
 *      Author: Max Kelly
 */

#ifndef 7SEGLED_H_
#define 7SEGLED_H_

#include "msp.h"


void Part1();
void Part2();
void Part3();
void delay_ms(float ms);
void segmentDisplay(int num);
void SysTick_Handler();


#endif /* 7SEGLED_H_ */
