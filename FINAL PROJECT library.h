/*
 * project.h
 *
 *  Created on: Dec 4, 2020
 *      Author: Max Kelly
 */

#ifndef PROJECT_H_
#define PROJECT_H_

void Door();
void Motor();
void Lights();

void CLEAR();

int keypressDC();

void pinSetup(void);
void SysTick_Setup();
void delay_micro(uint32_t micro);
void delay_ms(uint32_t milli);
void LCD_init(void);
void PulseEnablePin (void);
void pushNibble (uint8_t nibble);
void pushByte (uint8_t byte);
void commandWrite(uint8_t command);
void dataWrite(uint8_t data);
void writeString(char *myString);

void setupKeypad();
int getLastKeyPress();

#endif /* PROJECT_H_ */
