
/*
 * project.c
 *
 *  Created on: Dec 4, 2020
 *      Author: Max Kelly
 */

#include "msp.h"
#include <stdio.h>
#include <stdlib.h>

#include "project.h"

static int val = 0;


/*
 * Clear function set to clear LCD
 * Writes to LCD to display nothing but blank spaces
 * Returns nothing
 */
void CLEAR()
{
        char L1[50] = "                ";       //all chars are blank spaces so the lcd displays blank spaces
        char L2[50] = "                ";
        char L3[50] = "                ";
        char L4[50] = "                ";

        commandWrite(0x80);         //Writes to position on LCD
        delay_micro(100);           //Delays
        writeString(L1);

        commandWrite(0xC0);         //Writes to position on LCD
        delay_micro(100);           //Delays
        writeString(L2);

        commandWrite(0x90);         //Writes to position on LCD
        delay_micro(100);           //Delays
        writeString(L3);

        commandWrite(0xD0);         //Writes to position on LCD
        delay_micro(100);           //Delays
        writeString(L4);
}

/*
 * Door function set to open or close door and arm/disarm alarm
 * Writes to LCD to display menu and instructions
 * Returns nothing
 */
void Door()
{
    //variables used throughout key presses and while loop
    int key;
    int val = 1;    //important to reset val to 1 everytime door funciton is called

    //pwm pin setup
    P7 -> SEL0 |=  BIT5;
    P7 -> SEL1 &= ~BIT5;
    P7 -> DIR  |=  BIT5;

    P2->SEL0 &= ~BIT0; //sets P2.0 RED to 0
    P2->SEL1 &= ~BIT0; //everything on P2 is GPIO
    P2->DIR |= BIT0; // Sets direction to 1 which is output
    P2->OUT  |=  BIT0; //Set P2.0=1 (LED ON) TEST
//    P2->OUT  &= ~BIT0; // Turns LED off

    P2->SEL0 &= ~BIT1; //sets P2.0 GREEN to 0
    P2->SEL1 &= ~BIT1; //everything on P2 is GPIO
    P2->DIR |= BIT1; // Sets direction to 1 which is output
//  P2->OUT  |=  BIT1; //Set P2.0=1 (LED ON) TEST
    P2->OUT  &= ~BIT1; // Turns LED off

    P2->SEL0 |=  BIT4; //gpio to P5.6
    P2->SEL1 &= ~BIT4;
    P2->DIR  |=  BIT4; //direction is input (0)

    //pwm initialization for sounder
    TIMER_A0 -> CTL = 0b1000010100;
    TIMER_A0 -> CCR[0] = 3000-1;        //period
    TIMER_A0 -> CCTL[1] = 0b11100000;   //Reset/Set
    TIMER_A0 -> CCR[1] = 0;


    //chars made for changing lcd
    //char statusC[50] = "Door Menu:CLOSED";
    //char statusO[50] = "Door Menu:OPEN";
    char open[50] = "[1] Open";
    char close[50] = "[2] Close";
    char exit[50] = "[4] Exit";
    char arm[50] = "[3] Arm/disarm";

    char enterC[50] = "Enter Code";
    char armed[50] = "Alarm: armed";
    char disarmed[50] = "Alarm: disarmed";
    char IN[50] = "Only # to arm";
    char STRUCT[50] = "[#] Confirm";

   // char statusC[50] = "Door is: CLOSED";
  //  char statusO[50] = "Door is: OPEN";


printf("Made it to DOOR\n");        //check



commandWrite(0x80);         //Writes to position on LCD
delay_micro(100);           //Delays
writeString(open);

commandWrite(0xC0);         //Writes to position on LCD
delay_micro(100);           //Delays
writeString(close);

commandWrite(0x90);         //Writes to position on LCD
delay_micro(100);           //Delays
writeString(arm);

commandWrite(0xD0);         //Writes to position on LCD
delay_micro(100);           //Delays
writeString(exit);

//servo pwm initialization
TIMER_A1 -> CTL = 0b1000010100;
TIMER_A1 -> CCR[0] = 60000-1;        //period
TIMER_A1 -> CCTL[3] = 0b11100000;   //Reset/Set
TIMER_A1 -> CCR[3] = 60000*.03-1;    // duty cycle

//makes sure to start with door closed
P2->OUT  |=  BIT0;
P2->OUT  &= ~BIT1;

//variables made to work through door sequence
int alarm = 1;
int code;
int keyA;
int current = 0;
int count;

while(val == 1)
      {
          key = getLastKeyPress();  //get keypress
          if(key != -1)             //enters if a key is pressed
          {
              CLEAR();              //clears lcd

              printf("Key = %d\n",key); //check

              //door menu with instructions
              commandWrite(0x80);         //Writes to position on LCD
              delay_micro(100);           //Delays
              writeString(open);

              commandWrite(0xC0);         //Writes to position on LCD
              delay_micro(100);           //Delays
              writeString(close);

              commandWrite(0x90);         //Writes to position on LCD
              delay_micro(100);           //Delays
              writeString(arm);

              commandWrite(0xD0);         //Writes to position on LCD
              delay_micro(100);           //Delays
              writeString(exit);

              //if door set opened
              if(key == 1)
              {
                  printf("alarm is %d\n", alarm);   //check

                  //if alarm is armed
                  if(alarm == 1)
                  {
                      TIMER_A0 -> CCR[1] = 1500-1;    // duty cycle, alarm set on
                  }

                  //if alarm is disarmed
                  if(alarm == 0)
                  {
                      TIMER_A0 -> CCR[1] = 0;    // duty cycle, alarm sound is off

                  //servo moved to open postion
                  TIMER_A1 -> CTL = 0b1000010100;
                  TIMER_A1 -> CCR[0] = 60000-1;        //period
                  TIMER_A1 -> CCTL[3] = 0b11100000;   //Reset/Set
                  TIMER_A1 -> CCR[3] = 60000*.15-1;    // duty cycle

                  //Green LED turned on and red off
                  P2->OUT  |=  BIT1;
                  P2->OUT  &= ~BIT0;

                  }
              }

              //if door set closed
              if(key == 2)
              {
                  //if alarm is armed
                  if(alarm == 1)
                  {
                      TIMER_A0 -> CCR[1] = 1500-1;    // duty cycle
                  }

                  //if alarm is disarmed
                  if(alarm == 0)
                  {
                      TIMER_A0 -> CCR[1] = 0;    // duty cycle

                  //servo set to closed position
                  TIMER_A1 -> CTL = 0b1000010100;
                  TIMER_A1 -> CCR[0] = 60000-1;        //period
                  TIMER_A1 -> CCTL[3] = 0b11100000;   //Reset/Set
                  TIMER_A1 -> CCR[3] = 60000*.03-1;    // duty cycle

                  //red LED turned on and green off
                  P2->OUT  |=  BIT0;
                  P2->OUT  &= ~BIT1;


                  }
              }

              //if 4 pressed is exit button to go back to main menu
              if(key == 4)
              {
                  val = 0;  //val changed to exit while loop
                  CLEAR();  //lcd cleared

              }

              //enter a code to disarm
              if(key == 3)
              {
                  CLEAR();      //clears display

                  //Arm/disarm menu
                  commandWrite(0x80);         //Writes to position on LCD
                  delay_micro(100);           //Delays
                  writeString(enterC);

                  commandWrite(0x90);         //Writes to position on LCD
                  delay_micro(100);           //Delays
                  writeString(IN);

                  commandWrite(0xD0);         //Writes to position on LCD
                  delay_micro(100);           //Delays
                  writeString(STRUCT);

                  //if disarmed writes disarmed to lcd
                  if(alarm == 0)
                  {
                      commandWrite(0xC0);         //Writes to position on LCD
                      delay_micro(100);           //Delays
                      writeString(disarmed);
                  }

                  //if armed writes armed to lcd
                  if(alarm == 1)
                  {
                      commandWrite(0xC0);         //Writes to position on LCD
                      delay_micro(100);           //Delays
                      writeString(armed);
                  }

                  count = 1;

                  //while loop to lock in arm/disarm menu until confirmation button is pushed
                  while(count == 1)
                  {
                      keyA = getLastKeyPress();  //get keypress
                      delay_ms(50);             //debounce

                      if(keyA != -1)            //key is pressed
                      {

                          if(keyA != 20 && keyA != -2)              //if key is not # or *
                          {
                              current = current * 10 + keyA;        //lets user enter many numbers and saves them in order to variable current
                              printf("current is %d\n", current);   //check current

                          }

                          if(keyA == -2)        //# is pressed acting as confirmation code
                          {
                              code = current;   //change of variales


                              printf("press is %d\n", keyA);    //check

                          printf("code is %d\n", code);         //check

                          if(code == 1234)                      //1234 is password and if the code user enter matches then enter
                          {
                              //proceed
                              alarm = 0;                        //sets alarm to disarm

                              TIMER_A0 -> CCR[1] = 0;           // duty cycle, writes alarm pwm to 0

                          }

                          if(code != 1234)                      //if user code does match passcode then alarm set to arm
                          {
                              //TIMER_A0 -> CCR[1] = 1500-1;    // duty cycle

                              alarm = 1;
                          }
                          count = 0;            //exits while loop
                          current = 0;          //resets current for next entry
                          CLEAR();              //clears lcd for new menu

                          //new menu displayed for door
                          commandWrite(0x80);         //Writes to position on LCD
                          delay_micro(100);           //Delays
                          writeString(open);

                          commandWrite(0xC0);         //Writes to position on LCD
                          delay_micro(100);           //Delays
                          writeString(close);

                          commandWrite(0x90);         //Writes to position on LCD
                          delay_micro(100);           //Delays
                          writeString(arm);

                          commandWrite(0xD0);         //Writes to position on LCD
                          delay_micro(100);           //Delays
                          writeString(exit);

                          //writes door to closed and red LED on if passcode was incorrect
                          TIMER_A1 -> CTL = 0b1000010100;
                          TIMER_A1 -> CCR[0] = 60000-1;        //period
                          TIMER_A1 -> CCTL[3] = 0b11100000;   //Reset/Set
                          TIMER_A1 -> CCR[3] = 60000*.03-1;    // duty cycle

                          P2->OUT  |=  BIT0;
                          P2->OUT  &= ~BIT1;
                          }
                      }
                  }

              }

          }
      }
}

int i=0;
/*
 * Motor function set to change PWM of motor
 * Writes to LCD to display menu and instructions
 * Returns nothing
 */
void Motor()
{

    //chars made to write to the lcd when called
    char MM[50] = "Motor Menu";
    char mOFF[50] = "[0] Motor off";
    char High[50] = "[9] Highest speed";
    char Mexit[50] = "[#] Exit";

    printf("Made it to MOTOR\n");   //check

    //variables made to set motor speeds and work while loops
    int dc = 0;
    int k = 1;

       setupKeypad();

       //TA2.3
       P6 -> SEL0 |=  BIT6;
       P6 -> SEL1 &= ~BIT6;
       P6 -> DIR  |=  BIT6;

       //Setup for buttons that connects to ground
       P3->SEL0 &= ~BIT5; //gpio
       P3->SEL1 &= ~BIT5;
       P3->DIR  &= ~BIT5; //direction is input (0)
       P3->REN  |=  BIT5; //enable resistor
       P3->OUT  |=  BIT5; //set default state to a 1
       P3->IE   |=  BIT5;
       P3->IES  |=  BIT5;

           //sets up pwm registers
           TIMER_A2 -> CTL = 0b1000010100;
           TIMER_A2 -> CCR[0] = 37500-1;        //period
           TIMER_A2 -> CCTL[3] = 0b11100000;   //Reset/Set
           //TIMER_A2 -> CCR[3] = 0;    // duty cycle
           TIMER_A2 -> EX0 = 0b001;

           //writes motor menu with instructions
           commandWrite(0x80);         //Writes to position on LCD
           delay_micro(100);           //Delays
           writeString(MM);

           commandWrite(0xC0);         //Writes to position on LCD
           delay_micro(100);           //Delays
           writeString(mOFF);

           commandWrite(0x90);         //Writes to position on LCD
           delay_micro(100);           //Delays
           writeString(High);

           commandWrite(0xD0);         //Writes to position on LCD
           delay_micro(100);           //Delays
           writeString(Mexit);

           //locks in while loop until exit button is pressed and k is changed
       while (k==1)
       {

           //a if statement that only is called upon startup to make sure motor starts at 0
           if(i==0)
           {
               TIMER_A2 -> CCR[3] = 0;    // duty cycle

               i=1;
           }

           //keypressDC();
           dc = getLastKeyPress();  //get keypress
                      if(dc > -1)   //if key is pressed
                      {
                          //CLEAR();

                          printf("Key = %d\n",dc);          //check

                          TIMER_A2 -> CCR[3] = 3750*dc;    // duty cycle


                      }

                      //if # is pressed
                      if(dc == -2)
                      {
                          //TIMER_A2 -> CCR[3] = 0;    // duty cycle
                          k = 0;                        //used to exit while loop and return to main menu
                          CLEAR();                      // clears lcd for going back to main menu
                      }
       }
}

/*
 * Lights function set to change RGB
 * Writes to LCD to display menu and instructions
 * Returns nothing
 */
void Lights()
{

    //chars made for writing to lcd
    char LM[50] = "Lights Menu";
    char OPT[50] = "[1] [2] [3] [4]";
    char RGB[50] = " R   G   B  Exit";

    char RED[50] = "Red Brightness";
    char SelectP[50] = "Enter 0 - 100";
    char conf[50] = "[#] to confirm";

    char GREEN[50] = "Green Brightness";

    char BLUE[50] = "Blue Brightness";


    printf("Made it to LIGHTS\n");


    //green TA2.2
    P5 -> SEL0 |=  BIT7;
    P5 -> SEL1 &= ~BIT7;
    P5 -> DIR  |=  BIT7;

    //red   TA2.4
    P6 -> SEL0 |=  BIT7;
    P6 -> SEL1 &= ~BIT7;
    P6 -> DIR  |=  BIT7;

    //blue  TA3.1
    P10 -> SEL0 |=  BIT5;
    P10 -> SEL1 &= ~BIT5;
    P10 -> DIR  |=  BIT5;

    //variables used to set brightness
    int redBright, greenBright, blueBright;
    int key;
    int c = 1;          //reset c to 1 everytime funciton is entered
    int count;
    int current = 0;    //important to reset each time entered
    int pressR;
    int pressG;
    int pressB;

    //Setup for buttons that connects to ground
    P3->SEL0 &= ~BIT7; //gpio
    P3->SEL1 &= ~BIT7;
    P3->DIR  &= ~BIT7; //direction is input (0)
    P3->REN  |=  BIT7; //enable resistor
    P3->OUT  |=  BIT7; //set default state to a 1
    P3->IE   |=  BIT7;
    P3->IES  |=  BIT7;



    while(c == 1)
          {

        //writes menu
        commandWrite(0x80);         //Writes to position on LCD
        delay_micro(100);           //Delays
        writeString(LM);

        commandWrite(0x90);         //Writes to position on LCD
        delay_micro(100);           //Delays
        writeString(OPT);

        commandWrite(0xD0);         //Writes to position on LCD
        delay_micro(100);           //Delays
        writeString(RGB);

              key = getLastKeyPress();  //get keypress
              if(key != -1)
              {
                  delay_ms(300);                   //debounce

                  //RED
                  if(key == 1)
                  {
                      CLEAR();                    //clear lcd

                      commandWrite(0x80);         //Writes to position on LCD
                      delay_micro(100);           //Delays
                      writeString(RED);

                      commandWrite(0x90);         //Writes to position on LCD
                      delay_micro(100);           //Delays
                      writeString(SelectP);

                      commandWrite(0xD0);         //Writes to position on LCD
                      delay_micro(100);           //Delays
                      writeString(conf);

                     count = 1;
                     // int holderVAL;

                      while(count == 1)
                      {
                          pressR = getLastKeyPress();  //get keypress
                          delay_ms(50);                //debounce

                          if(pressR != -1)              //If key is pressed
                          {

                              if(pressR != 20 && pressR != -2)          //if key is not # or *
                              {
                                  current = current * 10 + pressR;      //saves a code to current, allows multiple presses to be put into one variable
                                  printf("current is %d\n", current);   //check

                              }

                              if(pressR == -2)              //if # is pressed for confirmation
                              {
                                  redBright = current;      //code set to brightness


                                  printf("press is %d\n", pressR);      //check

                              printf("red is %d\n", redBright);         //check

                              if(redBright > 100)       //error check
                              {
                                  printf("ERROR");
                              }

                              else                     //if value isn't over 100 sets brightness
                              {
                              TIMER_A2 -> CTL = 0b1000010100;
                              TIMER_A2 -> CCR[0] = 30000-1;        //period
                              TIMER_A2 -> CCTL[4] = 0b11100000;   //Reset/Set
                              TIMER_A2 -> CCR[4] = 300*redBright;    // duty cycle

                              }
                              count = 0;       //resets variables and clears lcd
                              current = 0;     //takes back to RGB menu
                              CLEAR();

                              }
                          }
                      }

                      //redBright = getLastKeyPress();  //get keypress

                     // printf("%d\n", redBright);
                  }




                  //GREEN
                  if(key == 2)
                  {
                       count = 1;

                       CLEAR();

                       commandWrite(0x80);         //Writes to position on LCD
                       delay_micro(100);           //Delays
                       writeString(GREEN);

                       commandWrite(0x90);         //Writes to position on LCD
                       delay_micro(100);           //Delays
                       writeString(SelectP);

                       commandWrite(0xD0);         //Writes to position on LCD
                       delay_micro(100);           //Delays
                       writeString(conf);
                                           // int holderVAL;

                       while(count == 1)
                       {
                           pressG = getLastKeyPress();  //get keypress
                           delay_ms(50);                //debounce

                           if(pressG != -1)             //if key is pressed
                           {

                               if(pressG != 20 && pressG != -2)         //if key is not # or *
                               {
                                   current = current * 10 + pressG;     //saves a code to current, allows multiple presses to be put into one variable
                                   printf("current is %d\n", current);  //check

                               }

                               if(pressG == -2)             //if # is pressed acting like the confirmation key
                               {
                                   greenBright = current;   //code set to brightness variable


                                   printf("press is %d\n", pressG); //check

                               printf("green is %d\n", greenBright);//check

                               if(greenBright > 100)    //error chekc
                               {
                                   printf("ERROR");     //value can't be over 100
                               }

                               else                     //if value isn't over 100 sets brightness
                               {
                               TIMER_A2 -> CTL = 0b1000010100;
                               TIMER_A2 -> CCR[0] = 30000-1;        //period
                               TIMER_A2 -> CCTL[2] = 0b11100000;   //Reset/Set
                               TIMER_A2 -> CCR[2] = 300*greenBright;    // duty cycle

                               }
                               count = 0;       //resets variables and clears lcd
                               current = 0;     //takes back to RGB menu
                               CLEAR();

                               }
                           }
                       }
                  }



                  //BLUE
                  if(key == 3)
                  {
                       count = 1;

                       CLEAR();

                       commandWrite(0x80);         //Writes to position on LCD
                       delay_micro(100);           //Delays
                       writeString(BLUE);

                       commandWrite(0x90);         //Writes to position on LCD
                       delay_micro(100);           //Delays
                       writeString(SelectP);

                       commandWrite(0xD0);         //Writes to position on LCD
                       delay_micro(100);           //Delays
                       writeString(conf);
                                           // int holderVAL;

                       while(count == 1)
                       {
                           pressB = getLastKeyPress();  //get keypress
                           delay_ms(50);

                           if(pressB != -1)         //if key is pressed
                           {

                               if(pressB != 20 && pressB != -2)     //if key is not # or *
                               {
                                   current = current * 10 + pressB; //saves a code to current, allows multiple presses to be put into one variable
                                   printf("current is %d\n", current);  //check

                               }

                               if(pressB == -2)         //if # was pressed and value is confirmed
                               {
                                   blueBright = current;    //brightness set to the value entered


                                   printf("press is %d\n", pressB); //check

                               printf("blue is %d\n", blueBright);  //check


                               if(blueBright > 100)     //checks if value is over 100
                               {
                                   printf("ERROR");     //value can't be over 100
                               }

                               else                     //if value isn't over 100 sets brightness
                               {
                               TIMER_A3 -> CTL = 0b1000010100;
                               TIMER_A3 -> CCR[0] = 30000-1;        //period
                               TIMER_A3 -> CCTL[1] = 0b11100000;   //Reset/Set
                               TIMER_A3 -> CCR[1] = 300*blueBright;    // duty cycle
                               }

                               count = 0;       //resets variables and clears lcd
                               current = 0;     //takes back to RGB menu
                               CLEAR();
                               }
                           }
                       }
                  }



                  if(key == 4)      //if statement to return to main menu and clear LCD
                  {
                      c = 0;
                      CLEAR();

                  }

              }
    }

}





/*
 * Pin set up function
 * Sets up all the pins correctly
 * Returns nothing
 */
void pinSetup()
{
    P4->SEL0 &= ~(0xFC);    //(0b11111100)
    P4->SEL1 &= ~(0xFC);    //GPIO
    P4->DIR |= (0xFC);      //Output
    P4->OUT &= ~(0xFC);     //Initially zero
}

/*
 * SysTick setup function
 * Sets up systick timer correctly
 * reutrns nothing
 */
void SysTick_Setup()
{
    SysTick -> CTRL = 0;            //Disable SysTick During step
    SysTick -> LOAD = 0x00FFFFFF;   //Max reload value
    SysTick -> VAL = 0;             //clears write
    SysTick -> CTRL = 0x00000005;   //Enable systick
}

/*
 * Delay Micro function
 * sets a delay in microseconds
 * Returns nothing
 */
void delay_micro(uint32_t micro)
{
SysTick -> LOAD = (micro*3-1);              //input delay * 3-1
SysTick -> VAL = 0;                         //clears write
while((SysTick -> CTRL & 0x00010000)==0);   //does until value is counted down from
}

/*
 * Delay milli function
 * Sets a delay in milliseconds
 * Returns nothing
 */
void delay_ms(uint32_t milli)
{
    SysTick -> LOAD = (milli*3000-1);           //load a delay of 3000 times sent delay time
    SysTick -> VAL = 0;                         //Clears write
    while((SysTick -> CTRL & 0x00010000)==0);   //Counts down from value to 0
}

/*
 * LCD_init function
 * calls commandwrite and delays
 * returns nothing
 */
void LCD_init(void)         //Initialization for LCD
{
    commandWrite(0x03);     //All values taken from lab instructions
    delay_ms(100);
    commandWrite(0x03);
    delay_micro(200);
    commandWrite(0x03);
    delay_ms(100);
    commandWrite(0x02);

    delay_micro(100);
    commandWrite(0x28);
    delay_micro(100);

    delay_micro(100);
    commandWrite(0x0F);
    delay_micro(100);
    commandWrite(0x01);
    delay_micro(100);
    commandWrite(0x06);
    delay_ms(100);
}

/*
 * Pulse Enable Pin function
 * calls delays
 * Returns nothing
 */
void PulseEnablePin(void)
{
    P4OUT &= ~BIT2;     // sets enable to 0 for output
    delay_micro(20);
    P4OUT |= BIT2;      //Sets enable to 1 for input
    delay_micro(20);
    P4OUT &= ~BIT2;     //enable back to output
    delay_micro(20);
}

/*
 * pushnibble function
 * calls pulseenable
 * returns nothing
 */
void pushNibble(uint8_t nibble)
{
    P4OUT &=~ 0xF0;                 //clears p4.4-p4.7
    P4OUT |= (nibble & 0x0F) << 4;  //pins p4.0-4 wired to D4-D7
    PulseEnablePin();               //calls pulseenable
}

/*
 * Pushbyte function
 * calls nibble twice for 8 bit number
 * reuturns nothing
 */
void pushByte(uint8_t byte)
{
    uint8_t nibble;                 //declares nibble

    nibble = (byte & 0xF0) >> 4;    //saves left 4 numbers of byte and pushes it 4 left
    pushNibble(nibble);             //sends that saved nibble to pushnibble

    nibble = byte & 0x0F;           //saves right 4 numbers of byte to nubble
    pushNibble(nibble);             //sends that new nibble to pushnibble
    delay_micro(100);
}

/*
 * CommandWrite function
 * clears RS and calls pushbyte
 * returns nothing
 */
void commandWrite(uint8_t command)
{
    P4->OUT &= ~BIT3;   //clears RS to 0
    pushByte(command);  //function with command parameter
}

/*
 * dataWrite function
 * sets RS to input and calls pushByte
 * Returns nothing
 */
void dataWrite(uint8_t data)
{
    P4->OUT |= BIT3;    //sets RS to 1
    pushByte(data);     //function with data parameter
}

/*
 * writeString Function
 * prints string that is sent to it
 * returns nothing
 */
void writeString(char *myString)
{
    int i=0;                        //initializing i
    while(myString[i] != '\0')      //loop through until string is done
    {
        dataWrite(myString[i]);     //Writes the character of the string using datawrite
        i++;                        //Gotta increment
    }
}



/*
*Pin setup initializes all pins correctly
*No parameters, not needed
*No return has nothing to give back
*/
void setupKeypad(void)
{
        P9->SEL0 &= ~(0x7F);    //(0b01111111)
        P9->SEL1 &= ~(0x7F);    //GPIO
        P9->DIR &= ~(0x7F);      //Output
        P9->REN |= (0x7F);      //Pull up resistor enabled
        P9->OUT |= (0x7F);     //Initially zero
}

/*
*getLastKeyPress function reads through a button being pressed and returns that pressed value
*no parameters, nothing is sent here
*returns value pressed
*/
int getLastKeyPress()
{
    if(!(ADC14->CTL0&BIT(16)))      //check if not busy
               {
               TIMER_A2 -> CCR[1] = (((ADC14->MEM[0]/16384.0)*100.0)*600.0);    // duty cycle       setting brightness of LCD backlight
               //printf("Counts = %d\n", ADC14->MEM[0]);        checks
              // printf("Counts = %d,    Voltage = %f\n", ADC14->MEM[0], ADC14->MEM[0]/16383.0*3.3);    checks
               }
    ADC14->CTL0 |= BIT0;

    int val = -1;

                P9->OUT &= ~BIT4;  //Set default state to a 0
                P9->DIR |=  BIT4;  //Change to an output (that is 0 due to previous line)
                                                                                            //Each button acts the same so I will comment one
                if((P9->IN & BIT0) == 0)                //If the button is pressed
                {
                    delay_ms(100);                      //SysTick delay
                    val = 1;
                    //while((P5->IN & BIT2) == 0);        //Lock as long as button is pressed so there is no reprints
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 4;
                   // while((P5->IN & BIT0) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 7;
                   // while((P1->IN & BIT7) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 20;
                   // while((P1->IN & BIT6) == 0);
                }

                P9->OUT |=  BIT4;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT4;  //Change to an input

                P9->OUT &= ~BIT5;  //Set default state to a 0
                P9->DIR |=  BIT5;  //Change to an output (that is 0 due to previous line)

                if((P9->IN & BIT0) == 0)
                {
                    delay_ms(100);
                    val = 2;
                   //while((P5->IN & BIT2) == 0);
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 5;
                   // while((P5->IN & BIT0) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 8;
                  //  while((P1->IN & BIT7) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = 0;
                 //   while((P1->IN & BIT6) == 0);
                }

                P9->OUT |=  BIT5;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT5;  //Change to an input

                P9->OUT &= ~BIT6;  //Set default state to a 0
                P9->DIR |=  BIT6;  //Change to an output (that is 0 due to previous line)

                if((P9->IN & BIT0) == 0)
                {
                    delay_ms(100);
                    val = 3;
                //    while((P5->IN & BIT2) == 0);
                }

                if((P9->IN & BIT1) == 0)
                {
                    delay_ms(100);
                    val = 6;
                 //   while((P5->IN & BIT0) == 0);
                }

                if((P9->IN & BIT2) == 0)
                {
                    delay_ms(100);
                    val = 9;
                 //   while((P1->IN & BIT7) == 0);
                }
                if((P9->IN & BIT3) == 0)
                {
                    delay_ms(100);
                    val = -2;
                  //  while((P1->IN & BIT6) == 0);
                }

                P9->OUT |=  BIT6;  //Set default state to a 1 (pull up)
                P9->DIR &= ~BIT6;  //Change to an input

    return val;
}
