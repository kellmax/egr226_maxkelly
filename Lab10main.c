/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          11/11/2020
*Assignment:    Lab 10: Interfacing with temp sensor and ADC
*File:          main.c
*Description:   To familiarize with ADC and temp sensor
*/
#include "msp.h"
#include <stdio.h>
#include "math.h"
#include "LCD.h"


/**
 * main.c
 */

// P8.5 = A20
// P5.5 = A0
void main(void)
{


    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    Part1();
   lab10();

}
