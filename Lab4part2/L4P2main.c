/*
*Course:        EGR 226: Microcontroller Programming and Applications
*Author:        Max Kelly
*Date:          9/30/2020
*Assignment:    Lab 4: Digital Inputs and Outputs
*File:          main.c
*Description:   To blink LED on MSP with button on MSP
*/

#include "msp.h"


/**
 * main.c
 */


int debounce();

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    P2->SEL0 &= ~BIT0; //sets P2.0 RED to 0
    P2->SEL1 &= ~BIT0; //everything on P2 is GPIO
    P2->DIR |= BIT0; // Sets direction to 1 which is output
//  P2->OUT  |=  BIT0; //Set P2.0=1 (LED ON) TEST
    P2->OUT  &= ~BIT0; // Turns LED off

    P2->SEL0 &= ~BIT1; //sets P2.0 GREEN to 0
    P2->SEL1 &= ~BIT1; //everything on P2 is GPIO
    P2->DIR |= BIT1; // Sets direction to 1 which is output
//  P2->OUT  |=  BIT1; //Set P2.0=1 (LED ON) TEST
    P2->OUT  &= ~BIT1; // Turns LED off


    P2->SEL0 &= ~BIT2; //sets P2.0 BLUE to 0
    P2->SEL1 &= ~BIT2; //everything on P2 is GPIO
    P2->DIR |= BIT2; // Sets direction to 1 which is output
//  P2->OUT  |=  BIT2; //Set P2.0=1 (LED ON) TEST
    P2->OUT  &= ~BIT2; // Turns LED off

    //Setup for a button that connects to ground
    P1->SEL0 &= ~BIT1; //gpio
    P1->SEL1 &= ~BIT1;
    P1->DIR  &= ~BIT1; //direction is input (0)
    P1->REN  |=  BIT1; //enable resistor
    P1->OUT  |=  BIT1; //set default state to a 1

    P2->OUT = 0; // Also sets all P2 pins to off

int count=0;    // Variable used to switch through if statements

    while(1)    // Infinite loop so the program keeps running through the code
       {

        printf("%d\n", count);      // A test print to check how code is working

        if ((P1->IN & BIT1) == 0)   // Checks to see if button is pushed
    {
            debounce();             // debounce used to double check to make sure that the button is pushed

            while((P1->IN & BIT1) == 0) // loop now contains all if statements because they are needed to run while button is pushed
            {
            count++;                    //count incresed right away from 0 to 1

            if(count==1){                   //Runs when count is 1
                P2->OUT |= BIT0;            //Sets red LED on and blue and green off
                P2->OUT &= ~BIT1;
                P2->OUT &= ~BIT2;
            }

            if(count==2){                   //Runs when count is 2
                P2->OUT |= BIT1;            //Sets green LED on and blue and red off
                P2->OUT &= ~BIT0;
                P2->OUT &= ~BIT2;
            }

            if(count==3){                   //Runs when count is 3
                P2->OUT |= BIT2;            // Sets blue LED on and blue and red off
                P2->OUT &= ~BIT0;
                P2->OUT &= ~BIT1;
                count = 0;                  //Resets count so that when the code loops back it goes to red again
            }
            __delay_cycles(3000000);        // hardcoded delay functions for one second to hold before turning next LED on
            }
    }

       }
}

int debounce(){             //debounce function
__delay_cycles(30000);      // Delay function hardcoded to delay slightly to double check if button is pushed
}







