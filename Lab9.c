#include "7SegLED.h"
#include "msp.h"
#include "math.h"
/*
 * lab9.c
 *
 *  Created on: Nov 2, 2020
 *      Author: Max Kelly
 */

int i=0;


/*
 * Part 1 function that sets everything up
 * returns nothing because it is a void
 */
void Part1()
{
    //Setup for buttons that connects to ground
    P3->SEL0 &= ~(0b11100000); //gpio
    P3->SEL1 &= ~(0b11100000);
    P3->DIR  &= ~(0b11100000); //direction is input (0)
    P3->REN  |=  (0b11100000); //enable resistor
    P3->OUT  |=  (0b11100000); //set default state to a 1
    P3->IE  |=  (0b11100000);   //Interrupt Enable
    P3->IES  |=  (0b11100000);

    P6 -> SEL0 |=  BIT6;    //Setting up PWM pin for DC motor
    P6 -> SEL1 &= ~BIT6;
    P6 -> DIR  |=  BIT6;

    TIMER_A2 -> CTL = 0b1000010100;
    TIMER_A2 -> CCR[0] = 37500-1;        //period
    TIMER_A2 -> CCTL[3] = 0b11100000;   //Reset/Set
    TIMER_A2 -> EX0 = 0b001;

    NVIC_EnableIRQ(PORT3_IRQn);
    __enable_interrupt();
    while(1);

}

/*
 * Part 2 function that calls 1 function
 * segmentDisplay called
 * returns nothing because it is a void
 */
void Part2()
{
P4 -> SEL0 &= ~(0b11111111);    //Setting up 7_segment LED
P4 -> SEL1 &= ~(0b11111111);    //binary used to set correct pins for ease
P4 -> DIR |= (0b11111111);
P4 -> OUT &= ~(0b11111111);     //initially all off
P4 -> REN |= (0b11111111);

SysTick->LOAD = 3000000;        //Setting up SysTick
SysTick->VAL = 0;
SysTick->CTRL = (BIT1|BIT0);  //Enable with interrupt

while(1)
{
        segmentDisplay(i);      // Calls display funciton
}

}

/*
 * Part 3 function that is basically mix between one and two
 * returns nothing because it is a void
 */
void Part3()
{
    //Setup for buttons that connects to ground
    P6->SEL0 &= ~(0b00000011); //gpio
    P6->SEL1 &= ~(0b00000011);
    P6->DIR  &= ~(0b00000011); //direction is input (0)
    P6->REN  |=  (0b00000011); //enable resistor
    P6->OUT  |=  (0b00000011); //set default state to a 1
    P6->IE  |=  (0b00000011);
    P6->IES  |=  (0b00000011);

    P4 -> SEL0 &= ~(0b11111111);    //Setting up 7_Segment LED
    P4 -> SEL1 &= ~(0b11111111);
    P4 -> DIR |= (0b11111111);
    P4 -> OUT &= ~(0b11111111);
    P4 -> REN |= (0b11111111);

    segmentDisplay(0);              //Makes sure display starts at 0

    NVIC_EnableIRQ(PORT6_IRQn);
    __enable_interrupt();
    while(1);
}

/*
 * Delay milli function
 * Sets a delay in milliseconds
 * Returns nothing
 */
void delay_ms(float ms)        //delays in milliseconds

{

    SysTick->LOAD = 3000*ms-1;          //Load is value we count to
                                        // -1 because we start at 0
    SysTick->VAL  = 0;                  //Starting value

    SysTick->CTRL |= BIT2|BIT0;                 //Turns the timer on

    while(!(SysTick->CTRL & 0x00010000));       //while its on and not equal to load value

    SysTick->CTRL = 0;                          //Turn off SysTick

}


void PORT6_IRQHandler()
{



    if((P6->IN & BIT0) == 0)            //if button is pushed
    {
        delay_ms(50);                   //debounce
        while((P6->IN & BIT0) == 0);    //lock while button is held
        i++;                            //increment
        if(i>9)                         //make sure i does not go over 9
            i = 0;                      //resets when going over 9
        segmentDisplay(i);              //prints i on display
    }

    if((P6->IN & BIT1) == 0)            //if button is pushed
    {
        delay_ms(50);                   //debounce
        while((P6->IN & BIT1) == 0);    //lock while button is held
        i--;                            //decrement
        if(i<0)                         //makes sure i does not go under 0
            i = 9;                      //resets i to loop up to 9
        segmentDisplay(i);              //prints i on display
    }

}

void PORT3_IRQHandler()
{
    int speed;


            if((P3->IN & BIT7) == 0)                //If button is pushed
            {
                delay_ms(50);                       //debounce
                while((P3->IN & BIT7) == 0);        //lock while button is pushed
                speed = speed + 1;                  //increment dc by 10 percent
                TIMER_A2 -> CCR[3] = 3750*speed;    //write new pwm
            }

            if((P3->IN & BIT6) == 0)
            {
                delay_ms(50);
                while((P3->IN & BIT6) == 0);
                speed = speed - 1;                  //decrement dc by 10 percent
                TIMER_A2 -> CCR[3] = 3750*speed;
            }

            if((P3->IN & BIT5) == 0)
            {
                delay_ms(50);
                while((P3->IN & BIT5) == 0);
                speed = 0;                          //set dc to 0, aka turn off motor
                TIMER_A2 -> CCR[3] = speed;
            }

            if(speed == 11)                         //if speed is set over 100% dc
            {
                speed = 9;                         //keep speed at 100%
                TIMER_A2 -> CCR[3] = speed;
            }

            if(speed == -1)                         //if speed goes below 0 dc
            {
                speed = 0;                          //keep speed at 0%
                TIMER_A2 -> CCR[3] = speed;
            }

            printf("%d\n", speed);                  //print for checking
}

/*
 * segmentDisplay function
 * turns on LED display for a number
 * Returns nothing
 */
void segmentDisplay(int num)
{
    P4 -> OUT &= ~(0b11111111);         //always reseting LED display to off before changing it

    if(num == 0)                        //the num sent is the num displayed each binary corresponds to correct segment
        P4 -> OUT |= (0b00111111);

    if(num == 1)
        P4 -> OUT |= (0b00000110);

    if(num == 2)
        P4 -> OUT |= (0b01011011);

    if(num == 3)
        P4 -> OUT |= (0b01001111);

    if(num == 4)
        P4 -> OUT |= (0b01100110);

    if(num == 5)
        P4 -> OUT |= (0b01101101);

    if(num == 6)
        P4 -> OUT |= (0b01111101);

    if(num == 7)
        P4 -> OUT |= (0b00000111);

    if(num == 8)
        P4 -> OUT |= (0b01111111);

    if(num == 9)
        P4 -> OUT |= (0b01101111);
}

void SysTick_Handler()
{
i++;                        //SysTick inturrupt just needs to increase i by one
}
